import React from "react";
import { RootNav } from "./navigation/RootNav";
import { Provider } from "react-redux";
import store from "./store";

export default function App() {
  return (
    <Provider store={store}>
      <RootNav />
    </Provider>
  );
}
