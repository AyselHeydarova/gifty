import React, { useState } from "react";
import { StyleSheet, TextInput, View, StatusBar } from "react-native";
import { Btn } from "../components/Btn";
import { ColorCircle } from "../components/ColorCircle";

import { connect } from "react-redux";
import { changeTextAndColor } from "../store/reducer";
import { COLORS } from "../styles/Colors";

export const MainScreen = connect(null, {
  changeTextAndColor,
})(({ navigation, changeTextAndColor }) => {
  const COLORSArray = [
    COLORS.orange,
    COLORS.red,
    COLORS.darkBlue,
    COLORS.purple,
    COLORS.green,
    COLORS.black,
  ];

  const [color, setColor] = useState("yellow");
  const [text, setText] = useState("");

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={COLORS.gray} />
      <TextInput
        style={styles.input}
        value={text}
        placeholder="Type your text"
        onChangeText={(v) => {
          setText(v);
        }}
      />
      <View style={styles.row}>
        {COLORSArray.map((color) => (
          <ColorCircle
            color={color}
            key={color}
            onPress={() => setColor(color)}
          />
        ))}
      </View>

      <Btn
        onPress={() => {
          changeTextAndColor({ color, text });
          navigation.navigate("details");
          setText("");
        }}
      />
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
    justifyContent: "center",
  },

  input: {
    height: 50,
    width: "100%",
    backgroundColor: "white",
    borderWidth: 4,
    borderColor: "#A8A8A8",
    paddingHorizontal: 20,
  },

  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 5,
  },
});
