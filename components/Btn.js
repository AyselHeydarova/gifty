import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
} from "react-native";

export const Btn = ({ title = "Send text", onPress }) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.btn}>
      <Text style={styles.btnText}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btn: {
    backgroundColor: "#27AE60",
    height: 57,
    width: Dimensions.get("window").width - 26,
    borderRadius: 9,
    marginHorizontal: 13,
    justifyContent: "center",
    position: "absolute",
    bottom: 15,
  },

  btnText: {
    color: "white",
    fontSize: 24,
    textAlign: "center",
  },
});
