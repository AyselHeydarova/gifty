import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";

export const ColorCircle = ({ onPress, color = "green" }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={{ ...styles.circle, backgroundColor: color }} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },

  circle: {
    backgroundColor: "#27AE60",
    height: 44,
    width: 44,
    borderRadius: 22,
    borderWidth: 3,
    borderColor: "#FFFFFF",
  },
});
