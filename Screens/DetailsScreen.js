import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { selectColor, selectText } from "../store/reducer";
import { connect } from "react-redux";
import { COLORS } from "../styles/Colors";

const mapStateToProps = (state) => ({
  color: selectColor(state),
  text: selectText(state),
});

export const DetailsScreen = connect(
  mapStateToProps,
  null
)(({ color, text }) => {
  return (
    <View style={[styles.container, { backgroundColor: color }]}>
      <Text style={styles.text}>Your text is: {text} </Text>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.black,
    justifyContent: "center",
  },
  text: {
    color: "white",
    fontSize: 24,
    marginHorizontal: 13,
  },
});
