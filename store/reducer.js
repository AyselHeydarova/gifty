const CHANGE_TEXT_AND_COLOR = "CHANGE_TEXT_AND_COLOR";

export const selectColor = (state) => state.color;
export const selectText = (state) => state.text;

export const changeTextAndColor = (payload) => ({
  type: CHANGE_TEXT_AND_COLOR,
  payload,
});

const initialState = {
  color: "red",
  text: "hello",
};

export function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case CHANGE_TEXT_AND_COLOR:
      return {
        ...state,
        text: payload.text,
        color: payload.color,
      };

    default:
      return state;
  }
}
