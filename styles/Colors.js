export const COLORS = {
  gray: "#505052",
  orange: "#D35400",
  red: "#C0392B",
  darkBlue: "#2C3E50",
  purple: "#8E44AD",
  green: "#27AE60",
  black: "#282828",
};
