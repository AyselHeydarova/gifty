import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { MainScreen } from "../Screens/MainScreen";
import { DetailsScreen } from "../Screens/DetailsScreen";

const { Navigator, Screen } = createStackNavigator();

export const RootNav = () => {
  return (
    <NavigationContainer>
      <Navigator screenOptions={{ title: "Gifty" }}>
        <Screen name="main" component={MainScreen} />
        <Screen name="details" component={DetailsScreen} />
      </Navigator>
    </NavigationContainer>
  );
};
